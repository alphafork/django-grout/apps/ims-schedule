from datetime import timedelta

from django.db.models import Case, Q, Value, When
from django.db.models.functions import Concat
from ims_workday.services import is_workday
from reusable_models import get_model_from_string

from .models import DailySchedule, ScheduleSlot

Room = get_model_from_string("ROOM")
Faculty = get_model_from_string("FACULTY")


def get_schedule_dates(start_date, end_date, weekdays):
    date = start_date
    date_list = []
    while date <= end_date:
        if date.weekday() in weekdays and is_workday(date):
            date_list.append(date)
        date += timedelta(1)
    return date_list


def is_schedule_overlapping(
    batch,
    date,
    slot=None,
    period=None,
    faculty=None,
    room=None,
    old_slot=None,
):
    if (slot and period) == (slot or period):
        raise AttributeError("Either 'slot' or 'period' needed, not both.")

    unavailable_slots = get_slots(batch, date, faculty, room, available=False)
    if old_slot:
        unavailable_slots = unavailable_slots.exclude(pk=old_slot.pk)

    if slot:
        return slot in unavailable_slots
    else:
        return unavailable_slots.filter(
            Q(start_time__range=period) | Q(end_time__range=period)
        ).exists()


def get_slots(batch, date, faculty=None, room=None, available=True):
    filter_queryset = {
        "schedule__batch": batch,
        "date": date,
    }
    if faculty:
        filter_queryset.update({"faculty": faculty})
    if room:
        filter_queryset.update({"room": room})
    current_slots = (
        DailySchedule.objects.filter(**filter_queryset)
        .order_by("slot")
        .values_list("slot", flat=True)
        .distinct()
    )
    used_slots = ScheduleSlot.objects.filter(pk__in=current_slots)
    unavailable_slots = set()
    for slot in used_slots:
        unavailable_slots = unavailable_slots.union(
            slot.get_overlapping_slots().values_list("pk", flat=True)
        )
    if available:
        return ScheduleSlot.objects.exclude(pk__in=unavailable_slots)
    return ScheduleSlot.objects.filter(pk__in=unavailable_slots)


def filter_schedule(slot=None, date=None, start_date=None, end_date=None, period=None):
    if (slot and period) == (slot or period):
        raise AttributeError("Either 'slot' or 'period' needed.")
    if date and (start_date or end_date):
        raise AttributeError("'date' is not accepted with 'start_date' and 'end_date'")

    period = period or (slot.start_time, slot.end_time)

    date_filter = {}
    if date:
        date_filter.update({"date": date})
    elif start_date and end_date:
        date_filter.update({"date__range": (start_date, end_date)})
    else:
        raise AttributeError(
            "Either 'date' or 'start_date' and 'end_date' is required."
        )
    return DailySchedule.objects.filter(
        Q(slot__start_time__range=period) | Q(slot__end_time__range=period)
    ).filter(**date_filter)


def get_rooms_and_faculties(
    slot=None, date=None, start_date=None, end_date=None, period=None, subject=None
):
    schedules = filter_schedule(slot, date, start_date, end_date, period)
    unavailable_faculties = schedules.values("faculty")
    unavailable_rooms = schedules.values("room")
    faculty_filter = {}
    if subject:
        faculty_filter.update({"subject": subject})
    faculties = (
        Faculty.objects.filter(**faculty_filter)
        .annotate(
            name=Concat(
                "staff__user__first_name",
                Value(" "),
                "staff__user__last_name",
            ),
            is_available=Case(
                When(pk__in=unavailable_faculties, then=False),
                default=True,
            ),
        )
        .values("pk", "name", "is_available")
    )
    rooms = Room.objects.annotate(
        is_available=Case(
            When(pk__in=unavailable_rooms, then=False),
            default=True,
        ),
    ).values("pk", "name", "is_available")
    return faculties, rooms
