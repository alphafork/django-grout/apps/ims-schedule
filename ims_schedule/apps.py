from django.apps import AppConfig


class IMSScheduleConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_schedule"

    model_strings = {
        "SCHEDULE": "Schedule",
        "SCHEDULE_SLOT": "ScheduleSlot",
        "DAILY_SCHEDULE": "DailySchedule",
    }
