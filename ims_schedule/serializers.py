import datetime

from django.utils.dates import WEEKDAYS
from ims_base.serializers import BaseModelSerializer
from rest_framework import serializers
from reusable_models import get_model_from_string

from .models import DailySchedule, ScheduleSlot
from .services import get_schedule_dates, is_schedule_overlapping

Batch = get_model_from_string("BATCH")
Subject = get_model_from_string("SUBJECT")
Faculty = get_model_from_string("FACULTY")
Room = get_model_from_string("ROOM")


class ScheduleSlotSerializer(BaseModelSerializer):
    def validate(self, attrs):
        validated_data = super().validate(attrs)
        start_time = validated_data.get("start_time", None)
        end_time = validated_data.get("end_time", None)
        if start_time > end_time:
            raise serializers.ValidationError(
                {"end_time": "End time can not be lesser than start time."}
            )
        return validated_data


class ScheduleSerializer(BaseModelSerializer):
    slot = serializers.PrimaryKeyRelatedField(
        write_only=True, required=False, queryset=ScheduleSlot.objects.all()
    )
    start_date = serializers.DateField(write_only=True, required=False)
    end_date = serializers.DateField(write_only=True, required=False)
    start_time = serializers.TimeField(write_only=True, required=False)
    end_time = serializers.TimeField(write_only=True, required=False)
    date = serializers.DateField(write_only=True, required=False)
    room = serializers.PrimaryKeyRelatedField(
        write_only=True, required=False, queryset=Room.objects.all()
    )
    faculty = serializers.PrimaryKeyRelatedField(
        write_only=True, queryset=Faculty.objects.all()
    )
    weekdays = serializers.MultipleChoiceField(
        write_only=True, choices=list(WEEKDAYS.items())
    )

    class Meta(BaseModelSerializer.Meta):
        extra_meta = {
            "room": {
                "related_model_url": "/room/room",
            },
            "faculty": {
                "related_model_url": "/faculty/faculty",
            },
            "slot": {
                "related_model_url": "/schedule/schedule-slot",
            },
        }

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        date = validated_data.get("date", None)
        start_date = validated_data.get("start_date", None)
        end_date = validated_data.get("end_date", None)
        start_time = validated_data.get("start_time", None)
        end_time = validated_data.get("end_time", None)
        weekdays = validated_data.get("weekdays", range(6))
        batch = validated_data.get("batch", None)
        slot = validated_data.get("slot", None)
        faculty = validated_data.get("faculty", None)
        room = validated_data.get("room", None)

        if start_date or end_date:
            if date:
                raise serializers.ValidationError(
                    {"date": "Date can not be present with Start Date or End Date."}
                )
            if not (start_date and end_date):
                field = "start_date" if end_date else "end_date"
                raise serializers.ValidationError(
                    {field: f"{field.capitalize()} can not be empty."}
                )

        period = None
        if slot:
            if start_time or end_time:
                raise serializers.ValidationError(
                    {"Either Slot or Start Time and End Time has to be given."}
                )
        elif not slot:
            if not (start_time and end_time):
                raise serializers.ValidationError(
                    {"Either Slot or Start Time and End Time has to be given."}
                )
            if start_time > end_time:
                raise serializers.ValidationError(
                    {"end_time": "End time can not be lesser than start time."}
                )
            period = (start_time, end_time)

        old_slot = None
        if self.instance:
            daily_schedules = self.instance.dailyschedule_set.order_by("date")
            old_slot = daily_schedules.first().slot
            if (
                daily_schedules.first().date < datetime.date.today()
                and daily_schedules.first().date != start_date
            ):
                raise serializers.ValidationError(
                    {"start_date": "Can not update schedule for a past date."}
                )
            if (
                daily_schedules.last().date < datetime.date.today()
                and daily_schedules.last().date != end_date
            ):
                raise serializers.ValidationError(
                    {"end_date": "Can not update schedule for a past date."}
                )
            if self.instance.batch != batch:
                raise serializers.ValidationError({"batch": "Batch can not be edited."})
            if (self.instance.dailyschedule_set.count() > 1) and date:
                raise serializers.ValidationError(
                    {"date": "Can not edit single date for a repeat schedule."}
                )
        if date:
            if date < datetime.date.today():
                raise serializers.ValidationError(
                    {"date": "Can not create schedule for a past date."}
                )
            if is_schedule_overlapping(
                batch,
                date,
                slot,
                period,
                faculty,
                room,
                old_slot,
            ):
                raise serializers.ValidationError(
                    {"date": "There is an overlapping schedule in this date."}
                )
        else:
            if start_date > end_date:
                raise serializers.ValidationError(
                    {"end_date": "End date can not be lesser than start date."}
                )
            if not self.instance and (start_date < datetime.date.today()):
                raise serializers.ValidationError(
                    {"start_date": "Can not create schedule for a past date."}
                )
            for date in get_schedule_dates(start_date, end_date, weekdays):
                if is_schedule_overlapping(
                    batch,
                    date,
                    slot,
                    period,
                    faculty,
                    room,
                    old_slot,
                ):
                    raise serializers.ValidationError(
                        {"Overlapping schedule(s) found in this period."}
                    )
        return validated_data

    def create(self, validated_data):
        slot = validated_data.pop("slot", None)
        start_date = validated_data.pop("start_date", None)
        end_date = validated_data.pop("end_date", None)
        date = validated_data.pop("date", None)
        start_time = validated_data.pop("start_time", None)
        end_time = validated_data.pop("end_time", None)
        faculty = validated_data.pop("faculty", None)
        room = validated_data.pop("room", None)
        weekdays = validated_data.pop("weekdays", range(6))

        schedule = super().create(validated_data)

        if not slot:
            slot = ScheduleSlot.objects.get_or_create(
                start_time=start_time,
                end_time=end_time,
                defaults={"is_custom": True},
            )[0]

        daily_schedule_data = {
            "faculty": faculty,
            "room": room,
            "slot": slot,
            "schedule": schedule,
        }

        if date:
            DailySchedule.objects.create(**{"date": date, **daily_schedule_data})
        else:
            for date in get_schedule_dates(start_date, end_date, weekdays):
                DailySchedule.objects.create(**{"date": date, **daily_schedule_data})
        return schedule

    def to_representation(self, instance):
        data = super().to_representation(instance)
        daily_schedules = instance.dailyschedule_set.all()
        if daily_schedules.count() == 1:
            data["date"] = daily_schedules.get().date
            data["repeat_schedule"] = False
        elif daily_schedules.count() > 1:
            data["start_date"] = daily_schedules.order_by("date").first().date
            data["end_date"] = daily_schedules.order_by("date").last().date
            data["repeat_schedule"] = True
        return data

    def update(self, instance, validated_data):
        start_date = validated_data.pop("start_date", None)
        end_date = validated_data.pop("end_date", None)
        start_time = validated_data.pop("start_time", None)
        end_time = validated_data.pop("end_time", None)
        date = validated_data.pop("date", None)
        slot = validated_data.pop("slot", None)

        instance_slot = instance.dailyschedule_set.first().slot
        daily_schedule_data = {}
        if not slot and (
            (instance_slot.start_time, instance_slot.start_time)
            != (start_time, end_time)
        ):
            slot = ScheduleSlot.objects.get_or_create(
                start_time=start_time,
                end_time=end_time,
                defaults={"is_custom": True},
            )[0]
        daily_schedule_data.update({"slot": slot})

        for item in ["room", "faculty"]:
            value = validated_data.get(item, None)
            if value and (value != getattr(instance, item, None)):
                daily_schedule_data.update({item: value})

        daily_schedules = instance.dailyschedule_set.order_by("date")

        if date:
            daily_schedule_data.update({"date": date})
            daily_schedules.update(**daily_schedule_data)
        else:
            daily_schedules.update(**daily_schedule_data)
            if daily_schedules.first().date != start_date:
                daily_schedules.filter(date__range=(datetime.date.today(), start_date))
            if daily_schedules.last().date != end_date:
                weekdays = validated_data.pop("weekdays", range(6))
                for date in get_schedule_dates(start_date, end_date, weekdays):
                    DailySchedule.objects.create(
                        **{"date": date, "schedule": instance, **daily_schedule_data}
                    )
        if not DailySchedule.objects.filter(slot=instance_slot).exists():
            if instance_slot.is_custom:
                instance_slot.delete()
        validated_data.pop("batch", None)
        return super().update(instance, validated_data)
