import datetime

from ims_base.apis import BaseAPIView, BaseAPIViewSet
from rest_framework.response import Response
from reusable_models import get_model_from_string

from ims_schedule.models import Schedule, ScheduleSlot
from ims_schedule.serializers import ScheduleSerializer
from ims_schedule.services import get_rooms_and_faculties

Batch = get_model_from_string("BATCH")
Subject = get_model_from_string("SUBJECT")
Faculty = get_model_from_string("FACULTY")
Room = get_model_from_string("ROOM")


class ScheduleSlotAPI(BaseAPIViewSet):
    def get_queryset(self):
        is_custom = self.request.query_params.get("is_custom", None)
        if is_custom == "False":
            return super().get_queryset().filter(is_custom=False)
        return super().get_queryset()


class ScheduleAPI(BaseAPIViewSet):
    serializer_class = ScheduleSerializer
    model_class = Schedule

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        daily_schedules = instance.dailyschedule_set.all()
        if daily_schedules.filter(date__lte=datetime.date.today()):
            daily_schedules.filter(date__gt=datetime.date.today()).delete()
        else:
            self.perform_destroy(instance)
        return Response(status=204)


class DailyScheduleAPI(BaseAPIViewSet):
    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.date <= datetime.date.today():
            return Response(status=403)
        self.perform_destroy(instance)
        return Response(status=204)


class ScheduleAvailabilityAPI(BaseAPIView):
    def get(self, request, **kwargs):
        date = request.query_params.get("date", None)
        start_date = request.query_params.get("start_date", None)
        end_date = request.query_params.get("end_date", None)
        start_time = request.query_params.get("start_time", None)
        end_time = request.query_params.get("end_time", None)
        subject = request.query_params.get("subject", None)
        slot = request.query_params.get("slot", None)

        if not date:
            if not (start_date and end_date):
                return Response(
                    status=400,
                    data="Either 'date' or 'start_date' and 'end_date' is required.",
                )

        period = None
        if not slot:
            if not (start_time and end_time):
                return Response(
                    status=400,
                    data="Either 'slot' or 'start_time' and 'end_time' is required.",
                )
            period = (start_time, end_time)
        else:
            slots = ScheduleSlot.objects.filter(pk=slot)
            if not slots.exists():
                return Response(
                    status=400,
                    data="Slot not found.",
                )
            slot = slots.first()

        faculties, rooms = get_rooms_and_faculties(
            slot, date, start_date, end_date, period, subject
        )

        return Response(
            {
                "faculty_availability": faculties,
                "room_availability": rooms,
            }
        )
