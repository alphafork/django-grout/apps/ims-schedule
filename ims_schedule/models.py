from django.db import models
from django.db.models import Q
from ims_base.models import AbstractLog
from reusable_models import get_model_from_string

Batch = get_model_from_string("BATCH")
Subject = get_model_from_string("SUBJECT")
Faculty = get_model_from_string("FACULTY")
Room = get_model_from_string("ROOM")


class Schedule(AbstractLog):
    batch = models.ForeignKey(Batch, on_delete=models.CASCADE)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.subject.__str__()

    def get_period(self):
        daily_schedules = self.dailyschedule.objects.all()
        if daily_schedules.count() == 1:
            return daily_schedules.get().date
        else:
            daily_schedules = daily_schedules.order_by("date")
            return (daily_schedules.first().date, daily_schedules.last().date)


class ScheduleSlot(AbstractLog):
    start_time = models.TimeField()
    end_time = models.TimeField()
    is_custom = models.BooleanField(default=False)

    def __str__(self) -> str:
        return f"{self.start_time} - {self.end_time}"

    def get_overlapping_slots(self):
        return ScheduleSlot.objects.filter(
            Q(start_time__range=(self.start_time, self.end_time))
            | Q(end_time__range=(self.start_time, self.end_time))
        )


class DailySchedule(AbstractLog):
    schedule = models.ForeignKey(Schedule, on_delete=models.CASCADE)
    slot = models.ForeignKey(ScheduleSlot, on_delete=models.CASCADE)
    date = models.DateField()
    faculty = models.ForeignKey(
        Faculty, on_delete=models.CASCADE, null=True, blank=True
    )
    room = models.ForeignKey(Room, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self) -> str:
        return self.schedule.__str__()
