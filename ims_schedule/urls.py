from django.urls import include, path
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from .apis import ScheduleAPI, ScheduleAvailabilityAPI

router = routers.SimpleRouter()
router.register(r"schedule", ScheduleAPI, "schedule")

urlpatterns = [
    path("schedule-availability/", ScheduleAvailabilityAPI.as_view()),
    path("", include(router.urls)),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=["json"])
